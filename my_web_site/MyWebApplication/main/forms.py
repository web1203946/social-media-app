from django import forms
from .models import User_Profile

class CreateNewPost(forms.Form):
    text = forms.CharField(label="post_text", max_length=500)

    class Meta:
        fields = ["post_text"]

class UserProfileForm(forms.ModelForm):
    class Meta:
        model = User_Profile
        fields = ['description', 'profile_pic']

class AccountSettingsForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(widget=forms.PasswordInput)
    email = forms.EmailField()