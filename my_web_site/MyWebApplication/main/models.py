from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Post(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="post", null=False)
    text = models.CharField(max_length=500, null=False)
    date = models.DateTimeField(auto_now_add=True)
    liked_by = models.ManyToManyField(User, related_name="liked_post", blank=True)
    was_edited = models.BooleanField(default=False)

    def total_likes(self):
        return self.liked_by.count()

    def __str__(self):
        return self.text

class Comment(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name="post_comment", null=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="comment", null=False)
    text = models.CharField(max_length=300)
    date = models.DateTimeField(auto_now_add=True)
    was_edited = models.BooleanField(default=False)

    def __str__(self):
        return self.text

class User_Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="user_profile", null=False)
    profile_pic =  models.ImageField(upload_to="main/files/profile_pic",default='default.jpg', blank=True)
    description = models.CharField(max_length=400, blank=True)
    def __str__(self):
        return self.user.username + " profile"

