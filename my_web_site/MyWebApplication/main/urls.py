from django.urls import path
from django.conf.urls.static import static
from django.conf import settings


from . import views

urlpatterns = [
    path('', views.home, name="home"),
    path('home/', views.home, name="home"),
    path('delete_post/<int:post_id>/', views.delete_post, name='delete_post'),
    path('edit_post/<int:post_id>/', views.edit_post, name='edit_post'),
    path('profile/<int:user_id>/', views.profile, name='profile'),
    path('edit_profile/<int:user_id>/', views.edit_profile, name='edit_profile'),
    path('account/settings/<int:user_id>', views.account_settings, name='account_settings'),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)