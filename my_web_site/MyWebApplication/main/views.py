from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, HttpRequest, HttpResponseRedirect
from .models import Post, Comment, User, User_Profile
from .forms import CreateNewPost , UserProfileForm, AccountSettingsForm
from django.http import JsonResponse
from django.contrib import messages

# Create your views here.

def home(request):
    user = request.user
    if user.is_authenticated and user.user_profile is None:
        user_profile = User_Profile(user=user)
        user_profile.save()
    show_post_creation_form = False
    post_list = Post.objects.all()
    post_list = post_list.order_by("-date")
    comment_list = Comment.objects.all()
    current_user = request.user
    if request.method == "POST":
        if request.POST.get("Show_create_post") == "clicked":
            show_post_creation_form = True
        if request.POST.get("cancel_post") == "clicked":
            show_post_creation_form = False
        if request.POST.get("create_post") == "clicked":
            print("create_post")
            user = request.user
            post_text = request.POST.get("post_text")
            post_text = post_text.rstrip()

            if len(post_text) > 0:
                post = Post(text=post_text, user=request.user)
                post.save()
            return HttpResponseRedirect("/home/")
        if request.POST.get("like_button") == "clicked":
            post_id = request.POST.get("post_id")
            post = get_object_or_404(Post, id=post_id)
            if post.liked_by.filter(id=user.id).exists():
                post.liked_by.remove(user)
            else:
                post.liked_by.add(user)
    return render(
        request,
        "main/home.html",
        {
            "show_post_creation_form": show_post_creation_form,
            "post_list": post_list,
            "comment_list": comment_list,
            "current_user": current_user,
        },
    )


def delete_post(request, post_id):
    post = get_object_or_404(Post, id=post_id)
    if request.user == post.user or request.user.is_superuser:
        post.delete()
    return redirect("home")


def edit_post(request, post_id):
    post = get_object_or_404(Post, id=post_id)
    if request.method == "POST":
        if request.POST.get("submit_edited_post") == "clicked":
            new_text = request.POST.get("edited_text")
            new_text = new_text.rstrip()
            if len(new_text) > 0:
                post.text = new_text
                post.was_edited = True
                post.save()
        return redirect("home")
    return render(request, "main/edit.html", {"post": post})

def profile(request, user_id):
    curent_user = request.user
    user = get_object_or_404(User, id=user_id)
    post_list = Post.objects.filter(user=user)
    post_list = post_list.order_by("-date")
    user_profile = User_Profile.objects.get(user=user)
    if request.method == "POST":
        if request.POST.get("like_button") == "clicked":
            post_id = request.POST.get("post_id")
            post = get_object_or_404(Post, id=post_id)
            if post.liked_by.filter(id=user.id).exists():
                post.liked_by.remove(user)
            else:
                post.liked_by.add(user)
    return render(request, "main/profile.html", {"user": user, "post_list": post_list, "user_profile": user_profile, "current_user": curent_user})

def edit_profile(request, user_id):
    if request.method == 'POST':
        form = UserProfileForm(request.POST, request.FILES, instance=request.user.user_profile)
        if form.is_valid():
            form.save()
            return redirect('profile', user_id=user_id)
    else:
        form = UserProfileForm(instance=request.user.user_profile)

    return render(request, "main/edit_profile.html", {'form': form})

def account_settings(request, user_id):
    if request.user.is_authenticated:
        user = get_object_or_404(User, id=user_id)
        if request.method == 'POST':
            form = AccountSettingsForm(request.POST)

            if form.is_valid():
                new_username = request.POST.get('username')
                new_password = request.POST.get('password')
                new_email = request.POST.get('email')

                user.username = new_username
                user.set_password(new_password)
                user.email = new_email
                user.save()

                return redirect('home')
            else:
                messages.error(request, 'Please correct the form errors.')
        else:
            form = AccountSettingsForm(initial={'username': user.username, 'email': user.email})

        return render(request, 'main/account_settings.html', {'user': user, 'form': form})