from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django import forms
from django.contrib.auth.models import User

class RegisterForm(UserCreationForm):
    email = forms.EmailField(
        max_length=100,
        required=True,
        help_text='Enter Email Address',
        widget=forms.TextInput(attrs={'class': 'form-control','placeholder':'Email Address'})
    )
    first_name = forms.CharField(
        max_length=200,
        required=True,
        help_text='Enter First Name',
        widget=forms.TextInput(attrs={'class': 'form-control','placeholder':'First Name'})
    )
    last_name = forms.CharField(
        max_length=200,
        required=True,
        help_text='Enter Last Name',
        widget=forms.TextInput(attrs={'class': 'form-control','placeholder':'Last Name'})
    )
    username = forms.CharField(
        max_length=200,
        required=True,
        help_text='Enter Username',
        widget=forms.TextInput(attrs={'class': 'form-control','placeholder':'Username'})
    )
    password1 = forms.CharField(
        max_length=200,
        required=True,
        help_text='Enter Password',
        widget=forms.PasswordInput(attrs={'class': 'form-control','placeholder':'Password'})
    )
    password2 = forms.CharField(
        max_length=200,
        required=True,
        help_text='Confirm Password',
        widget=forms.PasswordInput(attrs={'class': 'form-control','placeholder':'Confirm Password'})
    )
    check = forms.BooleanField(required=False) #this field creates a checkbox that must be checked to submit the form
    class Meta:
        model = User
        fields = [
            'username','email','first_name',
            'last_name','password1','password2','check',
        ]