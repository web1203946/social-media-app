from django.contrib.auth import views as auth_views
from django.urls import path , include
from . import views

urlpatterns = [
    path("logout/",views.custom_logout, name="custom_logout" ),
    path('terms/', views.terms, name='terms'),
]

