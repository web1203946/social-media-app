from django.shortcuts import render, redirect, HttpResponse, HttpResponseRedirect
from django.contrib.auth import login, authenticate
from django.contrib.auth import logout
from django.contrib.auth.forms import UserCreationForm
from .forms import RegisterForm
from django.contrib import messages
from main.models import User_Profile

# Create your views here.
def register(request):
    if request.method == "GET":
        form = RegisterForm()
        context = {"form": form}
        return render(request, "register/register.html", context)

    if request.method == "POST":
        form = RegisterForm(request.POST)

        if not request.POST.get("terms_conditions") == "clicked":
            form.add_error(
                "check",
                "You must accept the terms and conditions to continue.",
            )
            messages.error(request, "Please Accept Terms and Conditions")
            context = {"form": form}
            return render(request, "register/register.html", context)

        if form.is_valid():
            form.save()
            user = form.cleaned_data.get("username")
            messages.success(request, "Account was created for " + user)
            return redirect("/home")
        else:
            messages.error(request, "Error Processing Your Request")
            context = {"form": form}
            return render(request, "register/register.html", context)
    return render(request, "register/register.html", {})


def custom_logout(response):
    logout(response)
    return render(response, "registration/logout.html", {})


def terms(response):
    return render(response, "registration/terms.html", {})
